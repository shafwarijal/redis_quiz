import express from 'express';
import quizController from '../controllers/quizController.js';
import auth from '../middlewares/jwtMiddleware.js';

const router = express.Router();

router.get('/getQuiz', auth, quizController.getQuiz);
router.post('/postAnswer', auth, quizController.postAnswer);
router.post('/resetQuiz', auth, quizController.resetQuiz);
router.post('/resetScore', auth, quizController.resetScore);

export default router;
