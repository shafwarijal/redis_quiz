import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import condb from './databases/connect.js';
import { createClient } from 'redis';

import userRouter from './routes/userRouter.js';
import quizRouter from './routes/quizRouter.js';

dotenv.config();
const app = express();
const port = process.env.PORT || 9999;
const client = createClient();

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));

app.use('/api/user', userRouter);
app.use('/api/quiz', quizRouter);

client.on('error', (err) => console.log('Redis Client Error', err));

await client.connect().then(() => console.log('Connected to Redis'));

const startServer = async () => {
  try {
    condb(process.env.MONGODB_URL);

    app.listen(port, () => console.log(`Server is running on port ${port}`));
  } catch (error) {
    console.log(error);
  }
};

startServer();
