import jwt from 'jsonwebtoken';
// import userModel from '../databases/models/userModel.js';
import { Redis } from 'ioredis';
import { response } from '../helpers/responseHelper.js';

const redis = new Redis();

const auth = async (req, res, next) => {
  try {
    if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
      const token = req.headers.authorization.split(' ')[1];
      //   console.log(token);

      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      //   const user = await userModel.findById(decoded.id);
      const user = await redis.get(decoded.id);

      if (!user) return response(res, 401, 'Your session has been expired, please login again!');

      if (token !== JSON.parse(user).token) {
        return response(res, 403, 'Invalid JWT token!');
      }

      req.user = user;
      next();
    } else {
      response(res, 403, 'Invalid JWT token!');
    }
  } catch (error) {
    console.log(error);
    res.status(401).json({ message: error.message });
  }
};

export default auth;
