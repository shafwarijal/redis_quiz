import userModel from '../databases/models/userModel.js';
import { response } from '../helpers/responseHelper.js';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { Redis } from 'ioredis';

const redis = new Redis();

const userController = {
  signup: async (req, res) => {
    const { name, password, phone } = req.body;

    try {
      if (!name || !password || !phone) {
        return response(res, 400, 'Name, password and phone is required');
      }

      const checkUser = await userModel.findOne({ name });
      if (checkUser) {
        return response(res, 400, 'User already exists');
      }

      let hashedPassword = await bcrypt.hash(password, 10);
      const user = await userModel.create({
        name,
        password: hashedPassword,
        phone: phone,
        score: 0,
      });

      const { name: userName, score, phoneNumber, status } = user;
      response(res, 200, 'User created successfully', { name: userName, phone: phone, score, status });
    } catch (error) {
      console.log(error);
      response(res, 500, '', error.message);
    }
  },

  signin: async (req, res) => {
    const { name, password } = req.body;

    try {
      if (!name || !password) {
        return response(res, 400, 'Name and password is required');
      }

      const user = await userModel.findOne({ name });
      if (!user) {
        return response(res, 400, 'User not found');
      }

      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        return response(res, 400, 'Invalid password');
      }

      const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, { expiresIn: '1d' });

      await redis.setex(user._id, 60 * 60 * 5, JSON.stringify({ user, token }));

      response(res, 200, 'User logged in successfully', { user, token });
    } catch (error) {
      console.log(error);
      const errorDetails = error?.details?.[0]?.message;
      if (errorDetails) {
        return response(res, 400, errorDetails);
      }
      return response(res, 500);
    }
  },

  logout: async (req, res) => {
    try {
      const id = req.user.id;
      await del(username, 'token');
      response(res, 200, `Logout success! see you ${username}!`);
    } catch (err) {
      error(res, err);
    }
  },
};

export default userController;
