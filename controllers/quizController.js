import { Redis } from 'ioredis';
import userModel from '../databases/models/userModel.js';
import { getDataQuiz, getDataBucket } from '../helpers/dbHelper.js';
import { response } from '../helpers/responseHelper.js';
import { calculateHelper, calculateScorePerRound } from '../helpers/calculateHelper.js';
import { setRedisData, getRedisData, delRedisData } from '../helpers/redisHelper.js';
import { shuffledArray } from '../helpers/shuffleHelper.js';

const redis = new Redis();

let currentRound;
let roundPerScore = 0;

const quizController = {
  getQuiz: async (req, res) => {
    try {
      const loggedInUser = JSON.parse(req.user);
      const userId = loggedInUser?.user?._id;
      const phoneNumber = loggedInUser?.user?.phone;
      const user = await userModel.findById(userId);

      const lastCharacter = parseInt(phoneNumber.slice(-1));

      const currentRoundRedis = await getRedisData(`currentRound:${userId}`);

      if (currentRoundRedis === null) {
        setRedisData(`currentRound:${userId}`, lastCharacter, 259200);
      }

      currentRound = currentRoundRedis;

      if (currentRound === 'gameOver') {
        return response(res, 200, `Game Over! and your score is ${user.score}`);
      }

      const allQuiz = await getRedisData(`allQuiz:${userId}`);
      const bucketQuiz = await getRedisData(`bucketQuiz:${userId}`);
      const bucketQuizCurrent = await getRedisData(`bucketQuizCurrent:${userId}`);

      if (allQuiz && bucketQuiz && bucketQuizCurrent) {
      }

      if (allQuiz && bucketQuiz && bucketQuizCurrent) {
        const lengthBucket = 3 - bucketQuizCurrent.length;

        const firstAnsweredId = bucketQuizCurrent[0];
        const matchedQuiz = allQuiz.find((quiz) => quiz.id === firstAnsweredId);

        const data = {
          id: matchedQuiz.id,
          question: matchedQuiz.soal,
          answer: matchedQuiz.answer,
          quizzes: `${lengthBucket}/2`,
        };

        const currentQuestion = await getRedisData(`currentQuestion:${userId}`);

        if (!currentQuestion) {
          setRedisData(`currentQuestion:${userId}`, matchedQuiz, 259200);
        }

        const ttl = await redis.ttl(`currentQuestion:${userId}`);

        return res.status(200).json({
          data,
          ttl,
        });
      }

      const data = getDataQuiz();
      const dataBucket = getDataBucket();

      const bucketActive = dataBucket.quiz['round-' + lastCharacter];

      shuffledArray(bucketActive);

      setRedisData(`bucketQuizCurrent:${userId}`, bucketActive, 259200);
      setRedisData(`allQuiz:${userId}`, data.soal, 259200);
      setRedisData(`bucketQuiz:${userId}`, dataBucket.quiz, 259200);

      response(res, 200, 'Success generate quizzes!');
    } catch (error) {
      console.error('An error occurred:', error);
      response(res, 500, 'Internal Server Error');
    }
  },

  postAnswer: async (req, res) => {
    try {
      let scoreResult;
      const { answer } = req.body;

      const validAnswers = ['a', 'b', 'c', 'd'];

      if (!answer) {
        return response(res, 400, 'Answer is required');
      }

      if (answer === undefined || !validAnswers.includes(answer.toLowerCase())) {
        return response(res, 400, 'Field "answer" must be "a", "b", "c", or "d".');
      }

      const loggedInUser = JSON.parse(req.user);
      const userId = loggedInUser?.user?._id;
      const phoneNumber = loggedInUser?.user?.phone;

      const lastCharacter = parseInt(phoneNumber.slice(-1));

      const user = await userModel.findById(userId);

      const currentQuestion = await getRedisData(`currentQuestion:${userId}`);

      if (!currentQuestion) {
        return response(res, 400, 'No question available');
      }

      if (answer.toLowerCase() == currentQuestion.correctOption.toLowerCase()) {
        scoreResult = await calculateHelper(true, userId);
      } else {
        scoreResult = await calculateHelper(false, userId);
      }

      let message =
        answer.toLowerCase() == currentQuestion.correctOption.toLowerCase() ? 'Correct answer' : 'Incorrect answer';

      await userModel.updateOne({ _id: userId }, { score: user.score + scoreResult });

      const bucketQuizRedis = await redis.get(`bucketQuiz:${userId}`);
      const bucketQuiz = JSON.parse(bucketQuizRedis);

      const bucketQuizCurrent = await getRedisData(`bucketQuizCurrent:${userId}`);

      delRedisData(`currentQuestion:${userId}`);

      if (bucketQuizCurrent.length === 1) {
        const currentRoundRedis = await getRedisData(`currentRound:${userId}`);
        currentRound = currentRoundRedis;

        const isEligible = await calculateScorePerRound(roundPerScore);

        if (isEligible === true) {
          roundPerScore++;
          currentRound = (currentRound + 1) % 4;
          setRedisData(`currentRound:${userId}`, currentRound, 259200);
        }

        if (currentRound === lastCharacter && isEligible === true) {
          roundPerScore = 0;
          currentRound = 'gameOver';
          setRedisData(`currentRound:${userId}`, currentRound, 259200);
          const score = user.score + scoreResult;
          return response(res, 200, (message += '! Game Over! and your score is ' + score));
        }

        const bucketActive = bucketQuiz['round-' + currentRound];

        shuffledArray(bucketActive);

        setRedisData(`bucketQuizCurrent:${userId}`, bucketActive, 259200);

        if (isEligible === false) {
          return response(res, 200, (message += '! Game is Over! Sorry you failed to proceed to the next round!'));
        }
        return response(res, 200, message);
      }

      const remainingBucketQuiz = bucketQuizCurrent.filter((quiz) => quiz !== currentQuestion.id);

      setRedisData(`bucketQuizCurrent:${userId}`, remainingBucketQuiz, 259200);

      response(res, 200, message);
    } catch (error) {
      console.error('An error occurred:', error);
      response(res, 500, 'Internal Server Error');
    }
  },

  resetQuiz: async (req, res) => {
    try {
      const loggedInUser = JSON.parse(req.user);

      const userId = loggedInUser?.user?._id;
      const userName = loggedInUser?.user?.name;

      delRedisData(`allQuiz:${userId}`);
      delRedisData(`bucketQuiz:${userId}`);
      delRedisData(`bucketQuizCurrent:${userId}`);
      delRedisData(`currentQuestion:${userId}`);
      delRedisData(`currentRound:${userId}`);

      response(res, 200, `Success reset data user ${userName}`);
    } catch (error) {
      console.error('An error occurred:', error);
      response(res, 500, 'Internal Server Error');
    }
  },

  resetScore: async (req, res) => {
    try {
      const loggedInUser = JSON.parse(req.user);
      const userId = loggedInUser?.user?._id;
      const userName = loggedInUser?.user?.name;

      await userModel.updateOne({ _id: userId }, { score: 0 });

      response(res, 200, `Success reset score user ${userName}`);
    } catch (error) {
      console.error('An error occurred:', error);
      response(res, 500, 'Internal Server Error');
    }
  },
};

export default quizController;
