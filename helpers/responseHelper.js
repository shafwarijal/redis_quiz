export const response = (res, codeStatus, info, data) => {
  const handleMessage = () => {
    if (codeStatus === 201) return 'Success Without Data!';
    if (codeStatus === 400) return 'Bad Request!';
    if (codeStatus === 401) return 'Unauthorized!';
    if (codeStatus === 403) return 'Forbidden!';
    if (codeStatus === 404) return 'Not Found!';
    if (codeStatus === 500) return 'Internal Server Error!';
    return 'Success';
  };
  const generateTransactionId = () => {
    const timestamp = Date.now();
    const randomString = Math.random().toString(36).substring(2, 8).toUpperCase();

    return `TRX${timestamp}${randomString}`;
  };
  const responseData = {
    status: codeStatus,
    message: handleMessage(),
  };
  if (info) {
    responseData.info = info;
  }
  if (data) {
    responseData.data = data;
  }
  responseData.traansaction_id = generateTransactionId();
  return res.status(codeStatus).json(responseData);
};
