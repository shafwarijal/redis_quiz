import { Redis } from 'ioredis';

const redis = new Redis();

let score;
let scorePerRound = 0;
export const calculateHelper = async (answer, userId) => {
  //tambahkan calculate per round
  const ttl = await redis.ttl(`currentQuestion:${userId}`);
  const time = 60 * 5 - ttl;
  score = 0;

  if (!answer) {
    score = -5;
  } else {
    switch (true) {
      case time <= 10:
        score = 10;
        break;
      case time <= 20:
        score = 9;
        break;
      case time <= 30:
        score = 8;
        break;
      case time <= 40:
        score = 7;
        break;
      case time <= 50:
        score = 6;
        break;
      case time <= 60:
        score = 5;
        break;
      default:
        score = 0;
        break;
    }
  }

  scorePerRound += score;
  return score;
};

export const calculateScorePerRound = async (round) => {
  const bucketScore = [8, 12, 16, 20];

  if (round === 0 && scorePerRound >= bucketScore[0]) {
    scorePerRound = 0;
    return true;
  }
  if (round === 1 && scorePerRound >= bucketScore[1]) {
    scorePerRound = 0;
    return true;
  }
  if (round === 2 && scorePerRound >= bucketScore[2]) {
    scorePerRound = 0;
    return true;
  }
  if (round === 3 && scorePerRound >= bucketScore[3]) {
    scorePerRound = 0;
    return true;
  }
  scorePerRound = 0; // Set scorePerRound to 0 before returning false
  return false;
};
