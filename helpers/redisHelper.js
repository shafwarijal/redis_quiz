import Redis from 'ioredis';

const redis = new Redis();

export const setRedisData = async (key, value, expirationInSeconds) => {
  try {
    await redis.setex(key, expirationInSeconds, JSON.stringify(value));
    return true;
  } catch (error) {
    console.error('Error setting Redis data:', error);
    return false;
  }
};

export const getRedisData = async (key) => {
  try {
    const data = await redis.get(key);
    if (data) {
      return JSON.parse(data);
    }
    return null;
  } catch (error) {
    console.error('Error getting Redis data:', error);
    return null;
  }
};

export const delRedisData = async (key) => {
  try {
    await redis.del(key);
    return true;
  } catch (error) {
    console.error('Error deleting Redis data:', error);
    return false;
  }
};
