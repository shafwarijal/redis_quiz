import { readFileSync } from 'fs';

const dbPathQuiz = new URL('../databases/allQuiz.json', import.meta.url);
const dbPathBucket = new URL('../databases/bucket.json', import.meta.url);

export const getDataQuiz = () => JSON.parse(readFileSync(dbPathQuiz));
export const getDataBucket = () => JSON.parse(readFileSync(dbPathBucket));
// export const setData = (data) => writeFileSync(dbPath, JSON.stringify(data, null, 2));
